#!/usr/bin/env python
'''
auth ljh
2018-05-08 home
'''
# coding:gbk
#https://docs.python.org/2/tutorial/modules.html?highlight=import
#import random
#则应该为num = random.randint(1,100)
from random import *
#[Error] AttributeError: 'builtin_function_or_method' object has no attribute 'randint'
#这个报错咋回事？？？
#应该写成num = randint(1,100)
#This imports all names except those beginning with an underscore (_).

#from random import randint
#If the module name is followed by as, then the name following as is bound directly to the imported module.
#This is effectively importing the module in the same way that import fibo will do, with the only difference of it being available as fib.
#num = randint(1,100)

from random import randint as ok
#It can also be used when utilising from with similar effects:
num = ok(1,100)
while True:
    ans = float(input("猜猜我输入的是几："))
    if ans > num:
        print('你猜的太大了')
    elif ans < num:
        print('小了，我的哥')
    else:
        print('猜对了')
        break#如果不加这个，循环还是会继续不能停止