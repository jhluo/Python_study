# -*- coding: utf-8 -*-
'''
@author: ljh
0615 company
game 7-17. 编程实例 - 三人斗地主手牌生成
'''
import random
import time

#元祖，存放不变的牌
# 红桃，梅花，方片，黑桃
cardType = ("Heart  ", "Plum   ", "Diamond", "Spade  ")
cardNum = ("A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K")

def initcards():
	cardsLst = []#用于存放54张牌，每张牌由NUM+花色组成+大小王
	for num in cardNum:
		for type in cardType:
			cardsLst.append((num,type))
	cardsLst.append(("Queen","Red"))
	cardsLst.append(("Queen","Black"))
	#print(cardsLst)
	return cardsLst

def randomcards(cardsLst):
	#洗牌，打乱，存放为一个可变的列表random.shuffle
	random.shuffle(cardsLst)
	print(cardsLst)
	#利用列表可以求差集的形式进行取出
	'''
	lst1 = [1,3,5,6]
	lst2 = [1,3]
	lst3 = list(set(lst1).difference(set(lst2)))
	print(lst3)#[5, 6]
	'''
	#发牌,从乱序列表里面取出17张
	#random  a = random.sample(lst, 17)从lst中选择17个
	playerLst1 = random.sample(cardsLst, 17)#取出17张
	tempLst = list(set(cardsLst).difference(set(playerLst1)))#还剩下37张
	playerLst2 = random.sample(tempLst,17)#再次取出17张
	tempLst1 = list(set(tempLst).difference(set(playerLst2)))#还剩下20张
	playerLst3 = random.sample(tempLst1,17)#再次取出17张
	#使用列表解析式求交集
	#retE = [i for i in listB if i not in listA]
	tempLst2 = [i for i in tempLst1 if i not in playerLst3]#还剩下3张
	#print('player1：%r'%playerLst1)
	#print('player2：%r'%playerLst2)
	#print('player3：%r'%playerLst3)
	print('剩余的是：%r'%tempLst2)
	
	print("玩家1 手牌:", len(playerLst1))
	for card in playerLst1:
		print(' '.join(card))

	print("玩家2 手牌:", len(playerLst2))
	for card in playerLst2:
		print(' '.join(card))

	print("玩家3 手牌:", len(playerLst3))
	for card in playerLst3:
		print(' '.join(card))

	print("剩余底牌:", len(tempLst2))
	for card in tempLst2:
		print(' '.join(card))
def main():
	a=initcards()
	randomcards(a)
	pass

if __name__ == '__main__':
	main()