# -*- coding: utf-8 -*-
"""
@author: ljh
0528 lib
game 7-15. 编程实例 - 生成优惠券号码
【问题描述】

很多付费应用的开发者，会设计一些优惠券来吸引用户来使用新开发的应用，以达到一定的广告效应。

现在，请你帮他们设计并生成200个优惠券号码：

优惠码的字符由26个英文字符（大小写）组成
每个优惠码有8位
"""
'''
random.randint(a, b)返回一个整数N， a <= N <= b
random.choice()可以从任何序列，比如list列表中，选取一个随机的元素返回，可以用于字符串、列表、元组等。
random.randrange#Return a randomly selected element from range(start, stop, step).
random.shuffle()如果你想将一个序列中的元素，随机打乱的话可以用这个函数方法。
random.sample()可以从指定的序列中，随机的截取指定长度的片断，不作原地修改。

chr(int) 65-int-90是大写字母 chr(int) 97-int-122是小写字母
string.ascii_letters得到大小写字母的字符串
list(string.ascii_letters)字符串转换为列表
'''
import random,string
downList=['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']
upperList=['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']
def func1():
    checkcode = ""
    for i in range(8):
        list = [random.choice(downList)]+[random.choice(upperList)]
        current = random.choice(list)
        checkcode += str(current)
    #print(checkcode)
    return checkcode

def func2():
    checkcode=""
    for i in range(8):
        list = [chr(random.randrange(65, 90))] + [chr(random.randrange(97, 122))]
        current = random.choice(list)
        checkcode+=str(current)
    #print(checkcode)
    return checkcode

def func3():
    lst = list(string.ascii_letters)#利用string.ascii_letters产生原始列表
    random.shuffle(lst)#random.shuffle打乱列表
    #print(lst[:8])
    b = "".join(lst[:8])
    #print(b)
    return b

def func4():
    lst = list(string.ascii_letters)#利用string.ascii_letters产生原始列表
    a = random.sample(lst, 8)#random.sample随机选择lst中的8个，组成新的列表
    #print(a)
    b = ''.join(a)
    #print(b)
    return b
i = 0
while i<=20:
    i +=1
    func_num = random.randint(1,4)
    if func_num==1:
        print("func1 random.choice：%r"%func1())
    elif func_num==2:
        print("func2 random.randrange：%r" % func2())
    elif func_num==3:
        print("func3 random.shuffle：%r" % func3())
    elif func_num==4:
        print("func4 random.sample：%r" % func4())
    else:
        print("something wrong")