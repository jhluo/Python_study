'''
【问题描述】

BMI 指数（即身体质量指数，简称体质指数又称体重，英文为Body Mass Index，简称BMI），是用体重公斤数除以身高米数平方得出的数字

BMI < 18.5 体重偏轻
18.5 <= BMI < 24 体重正常
 BMI >= 24 体重偏重
设计一个BMI计算器吧，看看自己体重是否正常。

输入：身高、体重值

输出：BMI 指数、是否正常
'''
wg=eval(input("weight:"))
hg=eval(input("high:"))
# BMI=体重(千克)/(身高(米)*身高(米))
BMI = wg/(hg*hg)
if BMI < 18.5:
    print("BMI is:%.3f,体重偏轻"%BMI)
elif 18.5 <= BMI < 24:
    print("BMI is:%.3f,体重正常" % BMI)
else:
    print("BMI is:%d,体重偏重" % BMI)