In [18]: d = {'x': 1, 'y': 2, 'z': 3}

In [19]: d.items()#返回键值对的列表
Out[19]: [('y', 2), ('x', 1), ('z', 3)]

In [21]: d.values()
Out[21]: [2, 1, 3]

In [22]: d.popitem()
Out[22]: ('y', 2)

In [23]: d.popitem()#随机pop一个，pop之后d中会少一个
Out[23]: ('x', 1)


In [32]: d.keys()
Out[32]: ['y', 'x', 'z']#列表

In [33]: d.iterkeys()
Out[33]: <dictionary-keyiterator at 0x7f45d064e578># an iterator over the keys of d，迭代的对象，使用next
In [38]: dki = d.iterkeys()

In [39]: dki.next()
Out[39]: 'y'

In [40]: dki.next()
Out[40]: 'x'
同理：
In [48]: d.values()
Out[48]: [2, 1, 3]

In [49]: dkv = d.itervalues()

In [50]: dkv.next()
Out[50]: 2

In [51]: dkv.next()
Out[51]: 1

所以，遍历 key, value的两个方法是：
In [53]: for key, value in d.iteritems():
    ...:     print key,'corresponds to',d[key]
    ...:     
y corresponds to 2
x corresponds to 1
z corresponds to 3

In [54]: for key, value in d.items():
    ...:     print key,'equl to',d[key]
y equl to 2
x equl to 1
z equl to 3
