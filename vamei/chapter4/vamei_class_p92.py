# -*- coding: utf-8 -*-
'''
@author: ljh
0729 lib
从Python开始学编程 by vamei P92
'''
a = [1,2,3,4,5]
print(type(a))
#print(dir(list))#帮助查询一个类或者对象的所有属性
print(a.count(5))#计数，查看一共有多少元素5
print(a.index(1))#查看元素1出现时的下标
a.append(6)#最后插入一个元素
a.sort()#排序
a.reverse()#颠倒次序
print(a)#[6, 5, 4, 3, 2, 1]

print(a.pop())#去除最后一个元素并将该元素返回
a.remove(2)#去除第一次出现的元素2
a.insert(0,7)#第0号位置插入7
a.clear()#清空列表
print(a)


'''
元祖与字符串对象
'''
a = (1,3,5)
print(a.count(5))#5出现的次数
print(a.index(1))#1出现的下标
#字符串是特殊的元祖，但是可以修改，因为它删除原有字符串再建立新的字符串。  元祖始终是不可以改变的。
'''
Python 3.7.0 (v3.7.0:1bf9cc5093, Jun 27 2018, 04:59:51) [MSC v.1914 64 bit (AMD64)] on win32
>>> str = "Hello word"
>>> sub = "word"
>>> str.count(sub)#sub出现的次数
1
>>> str.find(sub)#sub第一次出现的位置，不包含则返回-1
6
>>> str.index(sub)#sub第一次出现的位置，不包含则举出错误
6
>>> str.rfind(sub)#从右边开始
6
>>> str.rindex(sub)#
6
>>> str.isalnum()#是否全部为数字或字母
False
>>> str.isalpha()#字母
False
>>> str.isdigit()#数字
False
>>> str.split()
['Hello', 'word']
>>> str.rsplit()
['Hello', 'word']
>>> str.rsplit('o',1)#关键字'o',最多分割1次。
['Hello w', 'rd']
>>> str.split('o',1)
['Hell', ' word']
>>> str.split('o',2)
['Hell', ' w', 'rd']
>>> str.join(sub)
'wHello wordoHello wordrHello wordd'
>>> str.replace(sub,'66')
'Hello 66'
>>> str.join(sub)
'wHello wordoHello wordrHello wordd'
>>>
'''