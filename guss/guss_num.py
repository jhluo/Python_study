'''
【问题描述】

在教程最开始的几课里，有一个猜数字的游戏：程序随机一个结果，用户通过命令行输入去猜。程序会告诉你猜大了还是小了，直到猜中为止。

现在，请你独立实现这个小游戏，并且加上一些功能，比如：

游戏可以反复进行，猜中了之后可以重新开始
统计用户猜了几轮，平均几次猜中
限制每轮猜的次数，判定输赢

'''
#!/usr/bin/env python
'''
auth ljh
2018-05-09 home
'''
# coding:gbk

from random import randint as rd
#It can also be used when utilising from with similar effects:
num = rd(1,10)
r_num = 0
times = 0
while True:
    flag = input('是否开始猜数字游戏？yes or no:')
    if flag=='yes':
        r_num += 1#计次数
        while True:
            ans = float(input("猜猜我输入的是几："))
            times += 1
            print('这是第%d轮'%times)
            if ans > num:
                print('你猜的太大了')
            elif ans < num:
                print('小了，我的哥')
            else:
                print('猜对了')
                break
    elif flag=='no':
        break
    if r_num > 0:
        print('一共猜了%d次，猜对了%d次'%(times,r_num))